import sqlite3
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By


driver = webdriver.Chrome()
driver.get("https://www.python.org/events/")

events = driver.find_elements(By.CLASS_NAME, "event-title")
events_list = []
for event in range(len(events)):
    events_list.append(events[event].text)
# print(events_list)

dates = driver.find_elements(By.TAG_NAME, "time")
dates_list = []
for date in range(len(dates)):
    dates_list.append(dates[date].text)
# print(dates_list)

locations = driver.find_elements(By.CLASS_NAME, "event-location")
locations_list = []
for location in range(len(locations)):
    locations_list.append(locations[location].text)
# print(locations_list)


events_dict = {}
for i in range(len(events)):
    events_dict[i] = [events_list[i], dates_list[i], locations_list[i]]

print(events_dict)


con = sqlite3.connect("tutorial.db")
cur = con.cursor()
# # cur.execute("CREATE TABLE event(title, date, location)")


# data = list(events_dict.values())
# print(data)
for i in range(len(events)):
    cur.execute(
        "INSERT INTO event VALUES(?, ?, ?)",
        (events_list[i], dates_list[i], locations_list[i]),
    )
    con.commit()
